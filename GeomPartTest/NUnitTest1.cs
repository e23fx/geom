﻿using System;
using NUnit.Framework;
using GeomPart;

namespace GeomPartTest
{
	[TestFixture]
	public class NUnitTest1
	{
		[Test]
		public void TestMethod1()
		{
			try
			{
				double area = GeomHelper.RightTriangleArea(0, 2, 2);
			}
			catch
			{
				Assert.AreEqual(true, true);
				return;
			}
			Assert.AreEqual(true, false);
		}

		[Test]
		public void TestMethod2()
		{
			try
			{
				double area = GeomHelper.RightTriangleArea(2, 2, 2);
			}
			catch
			{
				Assert.AreEqual(true, true);
				return;
			}
			Assert.AreEqual(true, false);
		}

		[Test]
		public void TestMethod3()
		{
			double area = GeomHelper.RightTriangleArea(3, 4, 5);
			Assert.AreEqual(area, 6);
		}

		[Test]
		public void TestMethod4()
		{
			double area = GeomHelper.RightTriangleArea(5, 3, 4);
			Assert.AreEqual(area, 6);
		}

		[Test]
		public void TestMethod5()
		{
			double area = GeomHelper.RightTriangleArea(4, 5, 3);
			Assert.AreEqual(area, 6);
		}

		[Test]
		public void TestMethod6()
		{
			try
			{
				double area = GeomHelper.RightTriangleArea(3, 4, -5);
			}
			catch
			{
				Assert.AreEqual(true, true);
				return;
			}
			Assert.AreEqual(true, false);
		}

		[Test]
		public void TestMethod7()
		{
			try
			{
				double area = GeomHelper.RightTriangleArea(double.MaxValue, 4, 5);
			}
			catch
			{
				Assert.AreEqual(true, true);
				return;
			}
			Assert.AreEqual(true, false);
		}
	}
}