﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeomPart
{
    public static class GeomHelper
    {
		/// <summary>
		/// Get Area of right triangle
		/// </summary>
		/// <param name="a">Side 1 length</param>
		/// <param name="b">Side 2 length</param>
		/// <param name="c">Side 3 length</param>
		/// <returns></returns>
		public static double RightTriangleArea(double a, double b, double c)
		{
			if(a <= 0 || b <= 0 || c <= 0)
			{
				throw new Exception("Incorrect triangle");
			}
			// Check triangle
			double sa = a*a;
			double sb = b*b;
			double sc = c*c;
			if(double.IsInfinity(sa)
				|| double.IsInfinity(sb)
				|| double.IsInfinity(sc))
            {
				throw new Exception("Incorrect triangle");
			}
			double firstSide;
			double secondSide;
			if (sa + sb == sc)
			{
				firstSide = a;
				secondSide = b;
			}
			else if (sa + sc == sb)
			{
				firstSide = a;
				secondSide = c;
			}
			else if (sb + sc == sa)
			{
				firstSide = b;
				secondSide = c;
			}
			else
			{
				throw new Exception("Incorrect triangle");
			}
			return firstSide * secondSide / 2;
		}
    }
}
